FROM nginx:1.25.3

COPY dist/tenjah-ihm/browser /usr/share/nginx/html 
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80